import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class ToDoList {

    private List<Task> taskList = new ArrayList<>();
    private Integer x = 0;
    Scanner scanner = new Scanner(System.in);

    void addToDoOne() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.println("Type task: ");
        String yourTask = br.readLine();
        x++;
        Task task = new Task(x, yourTask, false);
        taskList.add(task);
    }
    void viewTasks() {
        for (Task view : taskList) {
            System.out.println(view);
        }
    }

    void removeTask() {
        for (Task view : taskList) {
            System.out.println(view);
        }
        System.out.println("Choose task number to remove: ");
        int choice = scanner.nextInt();
        for (int i = 0; i < taskList.size(); i++) {
            Task removeTask = taskList.get(i);
            if (removeTask.getNumber().equals(choice)) {
                taskList.remove(i);
            }
        }
    }
    public void searchTask() throws IOException {
        String yourTask;

        Boolean check = false;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter task name: ");
        yourTask = input.next();

        System.out.println(" ");

        for (Task searchTask : taskList) {
            if (searchTask.getTask().equals(yourTask)) {
                System.out.println("Task number: " + searchTask.getNumber());
                check = true;
                break;
            }
        }
        if (check) {
            System.out.println("Searching done!");
            System.out.println(" ");
        } else {
            System.out.println("Not found task!");
            System.out.println(" ");
        }
    }
    public void updateTask() throws IOException {
        String yourTask;

        Boolean check = false;
        Scanner input = new Scanner(System.in);
        System.out.println("Updating task");
        System.out.print("Enter task: ");
        yourTask = input.next();
        System.out.println(" ");


        for (Task updateTask : taskList) {
            if (updateTask.getTask().equals(yourTask)) {
                System.out.println("Enter your new task number: ");
                int number = input.nextInt();
                updateTask.setNumber(number);
                check = true;
                break;
            }
        }
        if (check) {
            System.out.println("Task deleted!");
            System.out.println(" ");
        } else {
            System.out.println("Not found task!");
            System.out.println(" ");
        }
    }
    public void markMust() throws IOException {
        String yourTask;
        int number;
        Boolean check = false;
        Scanner input = new Scanner(System.in);
        System.out.println("Must marker ");
        System.out.print("Enter task: ");
        yourTask = input.next();
        System.out.println(" ");
        System.out.print("Enter number: ");
        number = input.nextInt();
        System.out.println(" ");

        for (Task updateTask : taskList) {
            if (updateTask.getTask().equals(yourTask) && updateTask.getNumber().equals(number)) {
                updateTask.setMust(true);
                check = true;
                break;
            }
        }
        if (check) {
            System.out.println("Task deleted!");
            System.out.println(" ");
        } else {
            System.out.println("Not found task!");
            System.out.println(" ");
        }
    }

    public void markMustShow() {
        Boolean check = false;

        for (int i = 0; i < taskList.size(); i++) {
            Task removeTask = taskList.get(i);
            if (removeTask.getMust().equals(true)) {
                System.out.println(" ");
                System.out.println("Task: " + taskList.get(i).getTask());

                System.out.println("Number of task: " + taskList.get(i).getNumber());
                System.out.println(" ");
                check = true;
                break;
            }
        }
        if (check) {
            System.out.println("Task deleted!");
            System.out.println(" ");
        } else {
            System.out.println("Not found task!");
            System.out.println(" ");
        }
    }
}
