import java.util.Objects;
public class Task {
    private Integer number;
    private String task;
    private Boolean must = false;

    public Task(Integer number, String task, Boolean must) {
        this.number = number;
        this.task = task;
        this.must = must;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task1 = (Task) o;
        return number.equals(task1.number) &&
                task.equals(task1.task) &&
                must.equals(task1.must);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, task, must);
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Boolean getMust() {
        return must;
    }

    public void setMust(Boolean must) {
        this.must = must;
    }

    @Override
    public String toString() {
        return number + " " + task +
                " " + must;
    }
}