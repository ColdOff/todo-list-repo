import java.io.IOException;
import java.util.Scanner;

public class ToDoTest {
    public static void main(String[] args) throws IOException {
        ToDoList toDoList = new ToDoList();
        Scanner input = new Scanner(System.in);
        int choose;
        System.out.println("              This is your 'ToDo' Notebook.");
        while (true) {
            System.out.println(" ");
            System.out.println("##################################################");
            System.out.println(" 1 - Add task");
            System.out.println(" 2 - Delete task");
            System.out.println(" 3 - View all tasks");
            System.out.println(" 4 - Search any task");
            System.out.println(" 5 - Update task");
            System.out.println(" 6 - Mark task as MustToDo");
            System.out.println(" 7 - Show all MustToDo tasks");
            System.out.println(" 8 - Quit");
            System.out.println("##################################################");
            System.out.println(" ");
            System.out.print("Choose what You want to do: ");
            System.out.println(" ");
            choose = input.nextInt();
            switch (choose) {
                case 1:
                    toDoList.addToDoOne();
                    break;
                case 2:
                    toDoList.removeTask();
                    break;
                case 3:
                    toDoList.viewTasks();
                    break;
                case 4:
                    toDoList.searchTask();
                    break;
                case 5:
                    toDoList.updateTask();
                    break;
                case 6:
                    toDoList.markMust();
                    break;
                case 7:
                    toDoList.markMustShow();
                    break;
                case 8:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Error");
                    break;
            }
        }
    }

}